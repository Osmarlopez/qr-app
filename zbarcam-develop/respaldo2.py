import os
from collections import namedtuple
import PIL
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ListProperty
from kivy.uix.anchorlayout import AnchorLayout
from pyzbar import pyzbar
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from kivy.config import Config
Config.set('graphics', 'resizable', True)
from .utils import fix_android_image
MODULE_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
global codi,etiqueta1,etiquetaf,validacion
etiqueta1 = []
etiquetaf = []
codi =  []
validacion = []
import pandas as pd
import time
from openpyxl import load_workbook
import openpyxl
import smtplib
from kivy.uix.label import Label





class ZBarCam(AnchorLayout):
    """
    Widget that use the Camera and zbar to detect qrcode.
    When found, the `codes` will be updated.
    """
    resolution = ListProperty([640, 480])

    symbols = ListProperty([])
    Symbol = namedtuple('Symbol', ['type', 'data'])
    # checking all possible types by default
    code_types = ListProperty(set(pyzbar.ZBarSymbol))

    def __init__(self, **kwargs):
        # lazy loading the kv file rather than loading at module level,
        # that way the `XCamera` import doesn't happen too early
        Builder.load_file(os.path.join(MODULE_DIRECTORY, "zbarcam.kv"))
        super().__init__(**kwargs)
        Clock.schedule_once(lambda dt: self._setup())

    def _setup(self):
        """
        Postpones some setup tasks that require self.ids dictionary.
        """
        self._remove_shoot_button()
        # `self.xcamera._camera` instance may not be available if e.g.
        # the `CAMERA` permission is not granted
        self.xcamera.bind(on_camera_ready=self._on_camera_ready)
        # camera may still be ready before we bind the event
        if self.xcamera._camera is not None:
            self._on_camera_ready(self.xcamera)

    def _on_camera_ready(self, xcamera):
        """
        Starts binding when the `xcamera._camera` instance is ready.
        """
        xcamera._camera.bind(on_texture=self._on_texture)

    def _remove_shoot_button(self):
        """
        Removes the "shoot button", see:
        https://github.com/kivy-garden/garden.xcamera/pull/3
        """
        xcamera = self.xcamera
        shoot_button = xcamera.children[0]
        xcamera.remove_widget(shoot_button)

    def _on_texture(self, instance):
        self.symbols = self._detect_qrcode_frame(
            texture=instance.texture, code_types=self.code_types)

    @classmethod
    def hello(self, instance):
        print("Funcion de  Comparacion de Arreglos")
        #codi.append("Maistro")
        #del(codi[0:-1])
        #del(etiqueta1[0:-1])
        #del(etiquetaf[0:-1])

        if len(validacion) ==1:
            validacion.append("2")
            del(validacion[1])

        validacion.append("1")
        print("Elementos validacion: ",validacion)
        print(codi)
        comparacion = []
        if len(validacion) == 3:
            for item in codi:
              if item in etiqueta1 and etiquetaf:
                comparacion.append(item)
            if len(comparacion) > 0:
              print('Ambas listas contienen estos elementos')
              for item in comparacion: print('%s' % item)
            if len(comparacion) >= len(codi):
              print('las 3 etiquetas  son iguales')
              layout = GridLayout(cols=1, padding=20)
              popupLabel = Label(text="Etiquetas Correctas")
              closeButton = Button(text="Continuar")
              layout.add_widget(popupLabel)
              layout.add_widget(closeButton)
              popup = Popup(title='Etiquetas Moldeo QR Trend Technologies México',
                            content=layout, size_hint=(None, None), size=(500, 500))
              popup.open()
              # Attach close button press with popup.dismiss action
              # closeButton.bind(on_press=cls.hello)
              closeButton.bind(on_press=popup.dismiss)



              del(codi[0:-1])
              del(etiqueta1[0:-1])
              del(etiquetaf[0:-1])
              del(validacion[0:-1])
              del(validacion[0])
              del(codi[0])
              del(etiqueta1[0])
              del(etiquetaf[0])

            else:
                print('Discrepancias en la eqtiquetas')


                layout = GridLayout(cols=1, padding=20)
                popupLabel = Label(text="Etiquetas Invalidas")
                closeButton = Button(text="Volver a Intentar")
                layout.add_widget(popupLabel)
                layout.add_widget(closeButton)
                popup = Popup(title='Etiquetas Moldeo QR Trend Technologies México',
                              content=layout, size_hint=(None, None), size=(500, 500))
                popup.open()
                # Attach close button press with popup.dismiss action
                # closeButton.bind(on_press=cls.hello)
                closeButton.bind(on_press=popup.dismiss)
                del(codi[0:-1])
                del(etiqueta1[0:-1])
                del(etiquetaf[0:-1])
                del(validacion[0:-1])

                del(validacion[0])
                del(codi[0])
                del(etiqueta1[0])
                del(etiquetaf[0])

    def _detect_qrcode_frame(cls, texture, code_types):
        image_data = texture.pixels
        size = texture.size
        # Fix for mode mismatch between texture.colorfmt and data returned by
        # texture.pixels. texture.pixels always returns RGBA, so that should
        # be passed to PIL no matter what texture.colorfmt returns. refs:
        # https://github.com/AndreMiras/garden.zbarcam/issues/41
        pil_image = PIL.Image.frombytes(mode='RGBA', size=size,
                                        data=image_data)
        pil_image = fix_android_image(pil_image)
        symbols = []
        codes = pyzbar.decode(pil_image, symbols=code_types)
        print(codes)
        elementos = len(codi)
        e1 = 0
        e2 = 0
        x = 1
        print("Datos finales: ",elementos,codi,etiqueta1,etiquetaf)

        if x >= elementos:
            for code in codes:
                symbol = ZBarCam.Symbol(type=code.type, data=code.data)
                symbols.append(symbol)
                if codi == []:
                    codigos = ""
                    #print("Etiqueta Incorrecta")
                    layout = GridLayout(cols=1, padding=20)
                    popupLabel = Label(text="Etiqueta de Estcion de Tranbajo Capturada")
                    closeButton = Button(text="Continuar")
                    layout.add_widget(popupLabel)
                    layout.add_widget(closeButton)
                    popup = Popup(title='Etiquetas Moldeo QR Trend Technologies México',
                                  content=layout,size_hint=(None, None), size=(500, 500))
                    popup.open()
                    # Attach close button press with popup.dismiss action
                    closeButton.bind(on_press=cls.hello)
                    closeButton.bind(on_press=popup.dismiss)
                    # time.sleep(5.5)

                    codigos = str(symbol[1])


                    print("Datos QR: ", codigos)
                    # resultado =
                    codi.append(codigos.split("|")[1])
                    codi.append(codigos.split("|")[2])
                    codi.append(codigos.split("|")[3])
                    codi.append(codigos.split("|")[4])
                    codi.append(codigos.split("|")[5])
                    codi.append(codigos.split("|")[6])
                    print("Etiqueta Base: ",codi)
                    e1 = 1
                    #time.sleep(10)
            print("Estos son los Elementos: ",codi, "& Val",validacion)


        if len(validacion) == 1:
            for code in codes:
                symbol = ZBarCam.Symbol(type=code.type, data=code.data)
                symbols.append(symbol)
                if codi != [] and etiqueta1 == []:
                    codigos = ""
                    # print("Etiqueta Incorrecta")
                    layout = GridLayout(cols=1, padding=20)
                    popupLabel = Label(text="Primera Etiqueta Capturada")
                    closeButton = Button(text="Continuar")
                    layout.add_widget(popupLabel)
                    layout.add_widget(closeButton)
                    popup = Popup(title='Etiquetas Moldeo QR Trend Technologies México',
                                  content=layout, size_hint=(None, None), size=(500, 500))
                    popup.open()
                    # Attach close button press with popup.dismiss action
                    closeButton.bind(on_press=cls.hello)
                    closeButton.bind(on_press=popup.dismiss)
                    # time.sleep(5.5)

                    codigos = str(symbol[1])
                    print("Datos QR: ", codigos)
                    codigos = ""
                    codigos = str(symbol[1])
                    print("Datos QR: ", codigos)
                    etiqueta1.append(codigos.split("|")[1])
                    etiqueta1.append(codigos.split("|")[2])
                    etiqueta1.append(codigos.split("|")[3])
                    etiqueta1.append(codigos.split("|")[4])
                    etiqueta1.append(codigos.split("|")[5])
                    etiqueta1.append(codigos.split("|")[6])
                    print("Primera etiqueta:", etiqueta1)
                    e2 = 1

        if len(validacion) == 2:
            for code in codes:
                symbol = ZBarCam.Symbol(type=code.type, data=code.data)
                symbols.append(symbol)
                if codi != [] and etiqueta1 != [] and etiquetaf ==[]:
                    codigos = ""
                    # print("Etiqueta Incorrecta")
                    layout = GridLayout(cols=1, padding=20)
                    popupLabel = Label(text="Ultima Etiqueta Capturada")
                    closeButton = Button(text="Continuar")
                    layout.add_widget(popupLabel)
                    layout.add_widget(closeButton)
                    popup = Popup(title='Etiquetas Moldeo QR Trend Technologies México',
                                  content=layout, size_hint=(None, None), size=(500, 500))
                    popup.open()
                    codigos = str(symbol[1])
                    print("Datos QR: ", codigos)
                    codigos = ""
                    codigos = str(symbol[1])
                    print("Datos QR: ", codigos)
                    etiquetaf.append(codigos.split("|")[1])
                    etiquetaf.append(codigos.split("|")[2])
                    etiquetaf.append(codigos.split("|")[3])
                    etiquetaf.append(codigos.split("|")[4])
                    etiquetaf.append(codigos.split("|")[5])
                    etiquetaf.append(codigos.split("|")[6])
                    # Attach close button press with popup.dismiss action
                    closeButton.bind(on_press=cls.hello)
                    closeButton.bind(on_press=popup.dismiss)
                    # time.sleep(5.5)


                    # datos.append(codigos[45:48])
                    print("Primera etiqueta:", etiquetaf)
                    e2 = 1

            print("lago de arreglo", len(symbols))
            #print(symbol[1])
        return symbols

    @property
    def xcamera(self):
        return self.ids['xcamera']

    def start(self):
        self.xcamera.play = True

    def stop(self):
        self.xcamera.play = False
